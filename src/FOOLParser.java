// $ANTLR 3.5.2 /home/ste/workspace/fool/FOOL.g 2017-03-02 18:38:06

import java.util.ArrayList;
import java.util.HashMap;
import ast.*;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ARROW", "ASS", "BOOL", 
		"CLASS", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR", "DIV", "DOT", 
		"ELSE", "EQ", "ERR", "EXTENDS", "FALSE", "FUN", "GE", "ID", "IF", "IN", 
		"INT", "INTEGER", "LE", "LET", "LPAR", "MINUS", "NEW", "NOT", "NULL", 
		"OR", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", 
		"WHITESP"
	};
	public static final int EOF=-1;
	public static final int AND=4;
	public static final int ARROW=5;
	public static final int ASS=6;
	public static final int BOOL=7;
	public static final int CLASS=8;
	public static final int CLPAR=9;
	public static final int COLON=10;
	public static final int COMMA=11;
	public static final int COMMENT=12;
	public static final int CRPAR=13;
	public static final int DIV=14;
	public static final int DOT=15;
	public static final int ELSE=16;
	public static final int EQ=17;
	public static final int ERR=18;
	public static final int EXTENDS=19;
	public static final int FALSE=20;
	public static final int FUN=21;
	public static final int GE=22;
	public static final int ID=23;
	public static final int IF=24;
	public static final int IN=25;
	public static final int INT=26;
	public static final int INTEGER=27;
	public static final int LE=28;
	public static final int LET=29;
	public static final int LPAR=30;
	public static final int MINUS=31;
	public static final int NEW=32;
	public static final int NOT=33;
	public static final int NULL=34;
	public static final int OR=35;
	public static final int PLUS=36;
	public static final int PRINT=37;
	public static final int RPAR=38;
	public static final int SEMIC=39;
	public static final int THEN=40;
	public static final int TIMES=41;
	public static final int TRUE=42;
	public static final int VAR=43;
	public static final int WHITESP=44;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/ste/workspace/fool/FOOL.g"; }


	private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();
	private int nestingLevel = -1;
	//livello ambiente con dichiarazioni piu' esterno � 0 (prima posizione ArrayList) invece che 1 (slides)
	//il "fronte" della lista di tabelle � symTable.get(nestingLevel)



	// $ANTLR start "prog"
	// /home/ste/workspace/fool/FOOL.g:24:1: prog returns [Node ast] : (e= exp SEMIC | LET d= declist IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> d =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:25:3: (e= exp SEMIC | LET d= declist IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||LA1_0==INTEGER||LA1_0==LPAR||LA1_0==NOT||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:25:11: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog49);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog51); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:27:11: LET d= declist IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog78); 
					nestingLevel++;
					             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					             symTable.add(hm);
					            
					pushFollow(FOLLOW_declist_in_prog107);
					d=declist();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog109); 
					pushFollow(FOLLOW_exp_in_prog113);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog115); 
					symTable.remove(nestingLevel--);
					             ast = new ProgLetInNode(d,e) ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "declist"
	// /home/ste/workspace/fool/FOOL.g:37:1: declist returns [ArrayList<Node> astlist] : ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ ;
	public final ArrayList<Node> declist() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:38:3: ( ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+ )
			// /home/ste/workspace/fool/FOOL.g:38:5: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			{
			astlist = new ArrayList<Node>() ;
			     int offset=-2;
			// /home/ste/workspace/fool/FOOL.g:40:9: ( ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC )+
			int cnt6=0;
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==FUN||LA6_0==VAR) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:40:11: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp ) SEMIC
					{
					// /home/ste/workspace/fool/FOOL.g:40:11: ( VAR i= ID COLON t= type ASS e= exp | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp )
					int alt5=2;
					int LA5_0 = input.LA(1);
					if ( (LA5_0==VAR) ) {
						alt5=1;
					}
					else if ( (LA5_0==FUN) ) {
						alt5=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 5, 0, input);
						throw nvae;
					}

					switch (alt5) {
						case 1 :
							// /home/ste/workspace/fool/FOOL.g:41:13: VAR i= ID COLON t= type ASS e= exp
							{
							match(input,VAR,FOLLOW_VAR_in_declist174); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist178); 
							match(input,COLON,FOLLOW_COLON_in_declist180); 
							pushFollow(FOLLOW_type_in_declist184);
							t=type();
							state._fsp--;

							match(input,ASS,FOLLOW_ASS_in_declist186); 
							pushFollow(FOLLOW_exp_in_declist190);
							e=exp();
							state._fsp--;

							VarNode v = new VarNode((i!=null?i.getText():null),t,e); 
							               astlist.add(v);
							               HashMap<String,STentry> hm = symTable.get(nestingLevel); 
							               if ( hm.put((i!=null?i.getText():null),new STentry(nestingLevel,t,offset--)) != null  ) // t
							                 {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);}  
							              
							}
							break;
						case 2 :
							// /home/ste/workspace/fool/FOOL.g:50:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= declist IN )? e= exp
							{
							match(input,FUN,FOLLOW_FUN_in_declist235); 
							i=(Token)match(input,ID,FOLLOW_ID_in_declist239); 
							match(input,COLON,FOLLOW_COLON_in_declist241); 
							pushFollow(FOLLOW_type_in_declist245);
							t=type();
							state._fsp--;

							//inserimento di ID nella symtable
							               FunNode f = new FunNode((i!=null?i.getText():null),t);
							               astlist.add(f);
							               HashMap<String,STentry> hm = symTable.get(nestingLevel);
							               STentry entry = new STentry(nestingLevel,offset--); //separo introducendo "entry"
							               if ( hm.put((i!=null?i.getText():null),entry) != null )
							                 {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
							                  System.exit(0);}
							                  //creare una nuova hashmap per la symTable
							               nestingLevel++;
							               HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
							               symTable.add(hmn);
							              
							match(input,LPAR,FOLLOW_LPAR_in_declist277); 
							ArrayList<Node> parTypes = new ArrayList<Node>();
							                    int paroffset=1;
							// /home/ste/workspace/fool/FOOL.g:66:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt3=2;
							int LA3_0 = input.LA(1);
							if ( (LA3_0==ID) ) {
								alt3=1;
							}
							switch (alt3) {
								case 1 :
									// /home/ste/workspace/fool/FOOL.g:66:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
									{
									fid=(Token)match(input,ID,FOLLOW_ID_in_declist301); 
									match(input,COLON,FOLLOW_COLON_in_declist303); 
									pushFollow(FOLLOW_type_in_declist307);
									fty=type();
									state._fsp--;

									 
									                  parTypes.add(fty); //
									                  ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
									                  f.addPar(fpar);
									                  if ( hmn.put((fid!=null?fid.getText():null),new STentry(nestingLevel,fty,paroffset++)) != null  )
									                    {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
									                     System.exit(0);}
									                  
									// /home/ste/workspace/fool/FOOL.g:75:19: ( COMMA id= ID COLON ty= type )*
									loop2:
									while (true) {
										int alt2=2;
										int LA2_0 = input.LA(1);
										if ( (LA2_0==COMMA) ) {
											alt2=1;
										}

										switch (alt2) {
										case 1 :
											// /home/ste/workspace/fool/FOOL.g:75:20: COMMA id= ID COLON ty= type
											{
											match(input,COMMA,FOLLOW_COMMA_in_declist348); 
											id=(Token)match(input,ID,FOLLOW_ID_in_declist352); 
											match(input,COLON,FOLLOW_COLON_in_declist354); 
											pushFollow(FOLLOW_type_in_declist358);
											ty=type();
											state._fsp--;


											                    parTypes.add(ty); //
											                    ParNode par = new ParNode((id!=null?id.getText():null),ty);
											                    f.addPar(par);
											                    if ( hmn.put((id!=null?id.getText():null),new STentry(nestingLevel,ty,paroffset++)) != null  )
											                      {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
											                       System.exit(0);}
											                    
											}
											break;

										default :
											break loop2;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_declist437); 
							entry.addType( new ArrowTypeNode(parTypes, t) );
							// /home/ste/workspace/fool/FOOL.g:87:15: ( LET d= declist IN )?
							int alt4=2;
							int LA4_0 = input.LA(1);
							if ( (LA4_0==LET) ) {
								alt4=1;
							}
							switch (alt4) {
								case 1 :
									// /home/ste/workspace/fool/FOOL.g:87:16: LET d= declist IN
									{
									match(input,LET,FOLLOW_LET_in_declist457); 
									pushFollow(FOLLOW_declist_in_declist461);
									d=declist();
									state._fsp--;

									match(input,IN,FOLLOW_IN_in_declist463); 
									}
									break;

							}

							pushFollow(FOLLOW_exp_in_declist469);
							e=exp();
							state._fsp--;

							//chiudere scope
							              symTable.remove(nestingLevel--);
							              f.addDecBody(d,e);
							              
							}
							break;

					}

					match(input,SEMIC,FOLLOW_SEMIC_in_declist500); 
					}
					break;

				default :
					if ( cnt6 >= 1 ) break loop6;
					EarlyExitException eee = new EarlyExitException(6, input);
					throw eee;
				}
				cnt6++;
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "declist"



	// $ANTLR start "type"
	// /home/ste/workspace/fool/FOOL.g:96:1: type returns [Node ast] : ( INT | BOOL );
	public final Node type() throws RecognitionException {
		Node ast = null;


		try {
			// /home/ste/workspace/fool/FOOL.g:97:3: ( INT | BOOL )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==INT) ) {
				alt7=1;
			}
			else if ( (LA7_0==BOOL) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:97:11: INT
					{
					match(input,INT,FOLLOW_INT_in_type547); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:98:11: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_type562); 
					ast =new BoolTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "exp"
	// /home/ste/workspace/fool/FOOL.g:101:1: exp returns [Node ast] : f= term ( PLUS l= term | MINUS l= term | OR l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:102:3: (f= term ( PLUS l= term | MINUS l= term | OR l= term )* )
			// /home/ste/workspace/fool/FOOL.g:102:5: f= term ( PLUS l= term | MINUS l= term | OR l= term )*
			{
			pushFollow(FOLLOW_term_in_exp588);
			f=term();
			state._fsp--;

			ast = f;
			// /home/ste/workspace/fool/FOOL.g:103:7: ( PLUS l= term | MINUS l= term | OR l= term )*
			loop8:
			while (true) {
				int alt8=4;
				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt8=1;
					}
					break;
				case MINUS:
					{
					alt8=2;
					}
					break;
				case OR:
					{
					alt8=3;
					}
					break;
				}
				switch (alt8) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:103:8: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp599); 
					pushFollow(FOLLOW_term_in_exp603);
					l=term();
					state._fsp--;

					ast = new PlusNode(ast,l);
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:105:9: MINUS l= term
					{
					match(input,MINUS,FOLLOW_MINUS_in_exp623); 
					pushFollow(FOLLOW_term_in_exp627);
					l=term();
					state._fsp--;

					ast = new MinusNode(ast,l);
					}
					break;
				case 3 :
					// /home/ste/workspace/fool/FOOL.g:107:9: OR l= term
					{
					match(input,OR,FOLLOW_OR_in_exp647); 
					pushFollow(FOLLOW_term_in_exp651);
					l=term();
					state._fsp--;

					ast = new OrNode(ast,l);
					}
					break;

				default :
					break loop8;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/ste/workspace/fool/FOOL.g:112:1: term returns [Node ast] : f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:113:3: (f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )* )
			// /home/ste/workspace/fool/FOOL.g:113:5: f= factor ( TIMES l= factor | DIV l= factor | AND l= factor )*
			{
			pushFollow(FOLLOW_factor_in_term692);
			f=factor();
			state._fsp--;

			ast = f;
			// /home/ste/workspace/fool/FOOL.g:114:7: ( TIMES l= factor | DIV l= factor | AND l= factor )*
			loop9:
			while (true) {
				int alt9=4;
				switch ( input.LA(1) ) {
				case TIMES:
					{
					alt9=1;
					}
					break;
				case DIV:
					{
					alt9=2;
					}
					break;
				case AND:
					{
					alt9=3;
					}
					break;
				}
				switch (alt9) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:114:8: TIMES l= factor
					{
					match(input,TIMES,FOLLOW_TIMES_in_term703); 
					pushFollow(FOLLOW_factor_in_term707);
					l=factor();
					state._fsp--;

					ast = new MultNode(ast,l);
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:116:10: DIV l= factor
					{
					match(input,DIV,FOLLOW_DIV_in_term728); 
					pushFollow(FOLLOW_factor_in_term732);
					l=factor();
					state._fsp--;

					ast = new DivNode(ast,l);
					}
					break;
				case 3 :
					// /home/ste/workspace/fool/FOOL.g:118:10: AND l= factor
					{
					match(input,AND,FOLLOW_AND_in_term753); 
					pushFollow(FOLLOW_factor_in_term757);
					l=factor();
					state._fsp--;

					ast = new AndNode(ast,l);
					}
					break;

				default :
					break loop9;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /home/ste/workspace/fool/FOOL.g:123:1: factor returns [Node ast] : f= value ( EQ l= value | LE l= value | GE l= value )* ;
	public final Node factor() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:124:3: (f= value ( EQ l= value | LE l= value | GE l= value )* )
			// /home/ste/workspace/fool/FOOL.g:124:5: f= value ( EQ l= value | LE l= value | GE l= value )*
			{
			pushFollow(FOLLOW_value_in_factor798);
			f=value();
			state._fsp--;

			ast = f;
			// /home/ste/workspace/fool/FOOL.g:125:7: ( EQ l= value | LE l= value | GE l= value )*
			loop10:
			while (true) {
				int alt10=4;
				switch ( input.LA(1) ) {
				case EQ:
					{
					alt10=1;
					}
					break;
				case LE:
					{
					alt10=2;
					}
					break;
				case GE:
					{
					alt10=3;
					}
					break;
				}
				switch (alt10) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:125:8: EQ l= value
					{
					match(input,EQ,FOLLOW_EQ_in_factor809); 
					pushFollow(FOLLOW_value_in_factor813);
					l=value();
					state._fsp--;

					ast = new EqualNode(ast,l);
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:127:9: LE l= value
					{
					match(input,LE,FOLLOW_LE_in_factor834); 
					pushFollow(FOLLOW_value_in_factor838);
					l=value();
					state._fsp--;

					ast = new LowerEqualNode(ast,l);
					}
					break;
				case 3 :
					// /home/ste/workspace/fool/FOOL.g:129:9: GE l= value
					{
					match(input,GE,FOLLOW_GE_in_factor859); 
					pushFollow(FOLLOW_value_in_factor863);
					l=value();
					state._fsp--;

					ast = new GreaterEqualNode(ast,l);
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "factor"



	// $ANTLR start "value"
	// /home/ste/workspace/fool/FOOL.g:134:1: value returns [Node ast] : (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | NOT LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? );
	public final Node value() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node fa =null;
		Node a =null;

		try {
			// /home/ste/workspace/fool/FOOL.g:135:3: (n= INTEGER | TRUE | FALSE | LPAR e= exp RPAR | NOT LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? )
			int alt14=8;
			switch ( input.LA(1) ) {
			case INTEGER:
				{
				alt14=1;
				}
				break;
			case TRUE:
				{
				alt14=2;
				}
				break;
			case FALSE:
				{
				alt14=3;
				}
				break;
			case LPAR:
				{
				alt14=4;
				}
				break;
			case NOT:
				{
				alt14=5;
				}
				break;
			case IF:
				{
				alt14=6;
				}
				break;
			case PRINT:
				{
				alt14=7;
				}
				break;
			case ID:
				{
				alt14=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 14, 0, input);
				throw nvae;
			}
			switch (alt14) {
				case 1 :
					// /home/ste/workspace/fool/FOOL.g:135:5: n= INTEGER
					{
					n=(Token)match(input,INTEGER,FOLLOW_INTEGER_in_value906); 
					ast = new IntNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// /home/ste/workspace/fool/FOOL.g:137:5: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_value923); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// /home/ste/workspace/fool/FOOL.g:139:5: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_value938); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// /home/ste/workspace/fool/FOOL.g:141:5: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_value952); 
					pushFollow(FOLLOW_exp_in_value956);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value958); 
					ast = e;
					}
					break;
				case 5 :
					// /home/ste/workspace/fool/FOOL.g:143:5: NOT LPAR e= exp RPAR
					{
					match(input,NOT,FOLLOW_NOT_in_value972); 
					match(input,LPAR,FOLLOW_LPAR_in_value974); 
					pushFollow(FOLLOW_exp_in_value978);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value980); 
					ast = new NotNode(e);
					}
					break;
				case 6 :
					// /home/ste/workspace/fool/FOOL.g:145:5: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_value994); 
					pushFollow(FOLLOW_exp_in_value998);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_value1000); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1002); 
					pushFollow(FOLLOW_exp_in_value1006);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1008); 
					match(input,ELSE,FOLLOW_ELSE_in_value1018); 
					match(input,CLPAR,FOLLOW_CLPAR_in_value1020); 
					pushFollow(FOLLOW_exp_in_value1024);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_value1026); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 7 :
					// /home/ste/workspace/fool/FOOL.g:148:5: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_value1041); 
					match(input,LPAR,FOLLOW_LPAR_in_value1043); 
					pushFollow(FOLLOW_exp_in_value1047);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_value1049); 
					ast = new PrintNode(e);
					}
					break;
				case 8 :
					// /home/ste/workspace/fool/FOOL.g:150:5: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_value1064); 
					//cercare la dichiarazione
					    int j=nestingLevel;
					    STentry entry=null; 
					    while (j>=0 && entry==null)
					      entry=(symTable.get(j--)).get((i!=null?i.getText():null));
					    if (entry==null)
					      {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
					       System.exit(0);}               
					    ast = new IdNode((i!=null?i.getText():null),entry,nestingLevel);
					// /home/ste/workspace/fool/FOOL.g:161:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					int alt13=2;
					int LA13_0 = input.LA(1);
					if ( (LA13_0==LPAR) ) {
						alt13=1;
					}
					switch (alt13) {
						case 1 :
							// /home/ste/workspace/fool/FOOL.g:161:7: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_value1086); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// /home/ste/workspace/fool/FOOL.g:162:7: (fa= exp ( COMMA a= exp )* )?
							int alt12=2;
							int LA12_0 = input.LA(1);
							if ( (LA12_0==FALSE||(LA12_0 >= ID && LA12_0 <= IF)||LA12_0==INTEGER||LA12_0==LPAR||LA12_0==NOT||LA12_0==PRINT||LA12_0==TRUE) ) {
								alt12=1;
							}
							switch (alt12) {
								case 1 :
									// /home/ste/workspace/fool/FOOL.g:162:8: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_value1100);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// /home/ste/workspace/fool/FOOL.g:163:9: ( COMMA a= exp )*
									loop11:
									while (true) {
										int alt11=2;
										int LA11_0 = input.LA(1);
										if ( (LA11_0==COMMA) ) {
											alt11=1;
										}

										switch (alt11) {
										case 1 :
											// /home/ste/workspace/fool/FOOL.g:163:10: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_value1113); 
											pushFollow(FOLLOW_exp_in_value1117);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop11;
										}
									}

									}
									break;

							}

							match(input,RPAR,FOLLOW_RPAR_in_value1146); 
							ast =new CallNode((i!=null?i.getText():null),entry,argList,nestingLevel);
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog49 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog51 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog78 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_prog107 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_prog109 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_prog113 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog115 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_declist174 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist178 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist180 = new BitSet(new long[]{0x0000000004000080L});
	public static final BitSet FOLLOW_type_in_declist184 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_ASS_in_declist186 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_declist190 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_FUN_in_declist235 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist239 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist241 = new BitSet(new long[]{0x0000000004000080L});
	public static final BitSet FOLLOW_type_in_declist245 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_declist277 = new BitSet(new long[]{0x0000004000800000L});
	public static final BitSet FOLLOW_ID_in_declist301 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist303 = new BitSet(new long[]{0x0000000004000080L});
	public static final BitSet FOLLOW_type_in_declist307 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_declist348 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_ID_in_declist352 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_COLON_in_declist354 = new BitSet(new long[]{0x0000000004000080L});
	public static final BitSet FOLLOW_type_in_declist358 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_declist437 = new BitSet(new long[]{0x0000042269900000L});
	public static final BitSet FOLLOW_LET_in_declist457 = new BitSet(new long[]{0x0000080000200000L});
	public static final BitSet FOLLOW_declist_in_declist461 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_IN_in_declist463 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_declist469 = new BitSet(new long[]{0x0000008000000000L});
	public static final BitSet FOLLOW_SEMIC_in_declist500 = new BitSet(new long[]{0x0000080000200002L});
	public static final BitSet FOLLOW_INT_in_type547 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_type562 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp588 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_PLUS_in_exp599 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp603 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_MINUS_in_exp623 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp627 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_OR_in_exp647 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_term_in_exp651 = new BitSet(new long[]{0x0000001880000002L});
	public static final BitSet FOLLOW_factor_in_term692 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_TIMES_in_term703 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term707 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_DIV_in_term728 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term732 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_AND_in_term753 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_factor_in_term757 = new BitSet(new long[]{0x0000020000004012L});
	public static final BitSet FOLLOW_value_in_factor798 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_EQ_in_factor809 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor813 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_LE_in_factor834 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor838 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_GE_in_factor859 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_value_in_factor863 = new BitSet(new long[]{0x0000000010420002L});
	public static final BitSet FOLLOW_INTEGER_in_value906 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_value923 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_value938 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_value952 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value956 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value958 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_value972 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value974 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value978 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value980 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_value994 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value998 = new BitSet(new long[]{0x0000010000000000L});
	public static final BitSet FOLLOW_THEN_in_value1000 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1002 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1006 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1008 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ELSE_in_value1018 = new BitSet(new long[]{0x0000000000000200L});
	public static final BitSet FOLLOW_CLPAR_in_value1020 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1024 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_CRPAR_in_value1026 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_value1041 = new BitSet(new long[]{0x0000000040000000L});
	public static final BitSet FOLLOW_LPAR_in_value1043 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1047 = new BitSet(new long[]{0x0000004000000000L});
	public static final BitSet FOLLOW_RPAR_in_value1049 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_value1064 = new BitSet(new long[]{0x0000000040000002L});
	public static final BitSet FOLLOW_LPAR_in_value1086 = new BitSet(new long[]{0x0000046249900000L});
	public static final BitSet FOLLOW_exp_in_value1100 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_COMMA_in_value1113 = new BitSet(new long[]{0x0000042249900000L});
	public static final BitSet FOLLOW_exp_in_value1117 = new BitSet(new long[]{0x0000004000000800L});
	public static final BitSet FOLLOW_RPAR_in_value1146 = new BitSet(new long[]{0x0000000000000002L});
}
